import argparse, os, socket
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", help="File to the proxy list")
parser.add_argument("-s", "--single", help="A single IP")
args = parser.parse_args()

class DNSBL:
    # settings
    print_ports = True
    each_blacklist = False
    finish_sound = False

    # variables 
    checkCount = ''
    totalAmounts = {"total":0, "notblacklisted":0, "blacklisted":0}

    def __init__(self):
        if args.single:
            if type(args.single) is not str:
                print "Error"
                return
            if len(args.single) < 5:
                print "Error"
                return
            self.check(args.single)
        elif args.file:
            if type(args.file) is not str:
                print "Error"
                return
            if self.checkProxyFile(args.file):
                self.start_up(args.file)
        else:
            self.prompt()

    def prompt(self):
        print "Please enter the path to the proxy list"
        file_link = raw_input("> ")
        if self.checkProxyFile(file_link):
            self.start_up(file_link)
        else:
            self.prompt()

    def start_up(self, file_loc):
        f = open(file_loc, "r")

        for theIP in f:
            theIP = theIP.replace("\n", "")
            self.totalAmounts["total"] += 1
            self.check(theIP)

        self.check("finish")

        if self.finish_sound:
            playSnd()

        print "%s IP's tested, %s blacklisted, %s not blacklisted." % (self.totalAmounts["total"], self.totalAmounts["blacklisted"], self.totalAmounts["notblacklisted"])

    def check(self, ip):
        found = False
        dnsbls = ["dnsbl.dronebl.org", "rbl.efnetrbl.org"]
        formattedIP = self.formatIP(ip)
        if not self.checkCount:
            self.checkCount = [formattedIP, 0]

        if ip == "finish":
            if not self.each_blacklist:
                if self.checkCount[1] == 0:
                    self.write(self.checkCount[0])
            else:
                self.write(self.checkCount[0])
            return

        if not self.each_blacklist:
            if self.checkCount[0] != formattedIP:
                if self.checkCount[1] == 0:
                    self.write(ip)
            self.checkCount[0] = formattedIP
            self.checkCount[1] = 0

        for dnsbl in dnsbls:
            h = "%s.%s" % (formattedIP, dnsbl)
            try:
                resolve = socket.gethostbyname(h)
                print "%s is listed with %s" % (ip, dnsbl)
                found = True
            except socket.gaierror:
                print "%s is not listed with %s" % (ip, dnsbl)
                if self.each_blacklist:
                    if self.checkCount[0] != formattedIP:
                        self.write(ip)
        if found:
            self.totalAmounts["blacklisted"] += 1
            self.checkCount[1] += 1
        else:
            self.totalAmounts["notblacklisted"] += 1
        found = False

    def formatIP(self, ip):
        if ":" in ip:
            ip = ip.split(":")[0]
        return '.'.join(ip.split('.')[::-1])

    def checkProxyFile(self, loc):
        if os.path.isfile(loc):
            try:
                f = open(loc)
                f.close()
                return True
            except IOError as e:
                print "Error: '%s'. Quitting.." % e
                return False
        else:
            print "'%s' not found, exiting" % loc
            return False

    def write(self, ip):
        fw = open ("notlisted.txt", "a")
        data = ip
        if self.print_ports:
            data = "%s \n" % ip
        else:
            if ":" in ip:
                data = "%s \n" % ip.split(":")[0]
            else:
                data = ip
        fw.write(data)

    def playSnd(self):
        import winsound, sys
        if "win" in sys.platform:
            winsound.PlaySound("SystemAterisk", winsound.SND_ALIAS)

DNSBL() 