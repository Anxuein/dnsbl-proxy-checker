# Usage
python dnsbl.py -f proxies.txt (proxies.txt being the list of the proxies)

or

python dnsbl.py -s 127.0.0.1 (being the IP you want to test, just one)

or

python dnsbl.py

# Settings
print_ports: When True, it will print ip:port. When False, it will print ip

each_blacklist: When True, if the IP is found in one blacklist but NOT in another, it will write it to the file.

finish_sound: Once finished play a sound (only works on Windows)

If you use multiple blacklists, keep it on false since it should pick up and grab the connecting IP and deny. Set False by default

# Other info
Coded by Anxuein

Licensed under the DBAD (Dont Be A Dick) Public License: http://www.dbad-license.org/